#include <Servo.h>
  
Servo s; // Variável Servo
int pos; // Posição Servo
int valor_analogico;
int aux = 0;
 
#define pino_sinal_analogico A0
#define SERVO 6 // Porta Digital 6 PWM

 

void setup()
{
  s.attach(SERVO);
  Serial.begin(9600);
  pinMode(pino_sinal_analogico, INPUT);
  s.write(90); // Inicia motor posição zero
  s.writeMicroseconds(1000);
}
 
void loop()
{
  //Le o valor do pino A0 do sensor
  valor_analogico = analogRead(pino_sinal_analogico);
 
  //Mostra o valor da porta analogica no serial monitor
  Serial.print("Porta analogica: ");
  Serial.print(valor_analogico);
 
  //Solo umido
  if (valor_analogico > 0 && valor_analogico < 400)
  {
      Serial.println(" Status: Solo umido");
      s.write(90); // Inicia motor posição zero
//      s.write(90);
    
  }
//  if(valor_analogico < 800 && valor_analogico > 400){
//    Serial.println("MEIO COISADO O SOLO!");
//    s.write(90); // Inicia motor posição zero
//     s.writeMicroseconds(1000);
//  }
  //Solo seco
  if (valor_analogico > 400 && valor_analogico < 1024)
  {
     Serial.println(" Status: Solo seco");
     s.write(90); // Inicia motor posição zero
     s.writeMicroseconds(1000);
  }
 
  
 
  delay(100);

}
