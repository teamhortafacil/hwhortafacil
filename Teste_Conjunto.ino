#include <Servo.h>
  
Servo s; // Variável Servo
int pos; // Posição Servo
int valor_analogico;
 
#define pino_sinal_analogico A0
#define SERVO 6 // Porta Digital 6 PWM
 

void setup()
{
  s.attach(SERVO);
  Serial.begin(9600);
  pinMode(pino_sinal_analogico, INPUT);
  s.write(0); // Inicia motor posição zero
}
 
void loop()
{
  //Le o valor do pino A0 do sensor
  valor_analogico = analogRead(pino_sinal_analogico);
 
  //Mostra o valor da porta analogica no serial monitor
  Serial.print("Porta analogica: ");
  Serial.print(valor_analogico);
 
  //Solo umido
  if (valor_analogico > 0 && valor_analogico < 400)
  {
      Serial.println(" Status: Solo umido");
      s.write(90);
  }
  //Solo seco
  if (valor_analogico > 800 && valor_analogico < 1024)
  {
      Serial.println(" Status: Solo seco");
      s.write(90);
  }
  delay(100);
}
